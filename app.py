#!/usr/bin/env

# ------------------------------------------------------------------------------
# imports
import json
import logging
import mvg_api
import requests
from datetime import datetime
from flask import Flask, render_template
from pytz import timezone, all_timezones
from typing import List


# ------------------------------------------------------------------------------
# utilities
def convert_to_my_tz(timestamp: int, tz: str = 'Europe/Berlin') -> datetime:
    """ Converts the given timestamp to a datetime obj of the provided timezone

    :param timestamp: expected UTC timestamp in ms
    :param tz: timezone as pytz timezone descriptor; default 'Europe/Berlin'
    :return: datetime obj of the timestamp in the provided timezone
    """

    # converting timestamp to datetime obj with utc timezone
    utc_time = datetime.utcfromtimestamp(timestamp)
    utc_time = timezone('UTC').localize(utc_time)

    # converting utc datetime obj to desired timezone if possible
    time = utc_time.astimezone(timezone(is_tz(tz)))

    return time


def format_datetime(dt: datetime, format: str) -> datetime:
    """ Formatting a given datetime object in the provided way

    :param dt: datetime obj to be formatted
    :param format: format to apply
    :return: formatted datetime obj or 0 if format not applicable
    """
    try:
        return dt.strftime(format)
    except:
        return 0


def get_time_diff(start_time: datetime, end_time: datetime, base: int = 60):
    """ Returns the time difference in units based on the base argument

    :param start_time: start time as datetime obj
    :param end_time: start time as datetime obj
    :param base: quotient as base for formatting; defaults to 60 as minutes
    :return: returns the time difference as tuple with the base units
            (e.g. seconds) as last element and then going up, e.g. (hour, min, sec)
    """

    time_diff = (end_time - start_time).total_seconds()
    return divmod(time_diff, base)


def time_to_dep(time_diff: tuple) -> str:
    """ Returning the remaining time in minutes and seconds as string to be displayed

    :param time_diff: tuple with the format (minutes, seconds)
    :return:
    """

    time_left = 'in {} Minuten und {} Sekunden'.format(time_diff[0], time_diff[1])
    return time_left


def is_tz(tz: str):
    """ Checks if provided timezone is supported and returns Berlin timezone otherwise

    :param tz: selected timezone to be used
    :return: either tz or Berlin timezone
    """

    if tz in all_timezones:
        return tz
    else:
        logging.warning('provided timezone {} not supported. Using Europe/Berlin'.format(tz))
        return 'Europe/Berlin'


def log_response(resp, kind, config):
    """ Writing the content of resp to the applicable file for logging

    :param resp: content to be logged
    :param kind: kind of the information which will determine where it will be saved
    :param config: config parser
    """

    log_options = {'weather': config['weather'].getpath('log_file')}

    # if the kind of log is supported, write resp with a time to the configured log file
    if kind in log_options.keys():
        # TODO: open file and save resp to it
        # logging.info('in logging mode')
        # logging.info('Saving logging to path: {}'.format(log_options['weather']))
        # with open(log_options[kind]) as log_file:
        #     data = json.load(log_file)
        # with open(log_options[kind], 'w+') as log_file:
        #     now = datetime.now(timezone(is_tz(config['global'].get('timezone'))))
        #     log = {'Date': now.strftime('%Y-%m-%d %H:%M:%S'),
        #            'Data': resp}
        #     data = data.update(log)
        #     print(data)
        #     json.dump(data, log_file, indent=4)
        pass
    else:
        logging.warning('Logging kind is not supported. Info is not logged!')


def get_weather_info(id: int, key: str, units: str) -> json:
    """ Requesting weather information for given city id at openweathermap.org

    :param id: city id according to openweathermap.org
    :param key: api key
    :param units: units the weather shall be in
    """

    req = 'http://api.openweathermap.org/data/2.5/weather?id={}&APPID={}&units={}' \
        .format(id, key, units)

    try:
        response = requests.get(req)
        # TODO: log the received response
        # log_response(response.json(), 'weather', config)

        if response.status_code == 200:
            return response
        else:
            return 'No weather available! :/'
    except:
        logging.info('No connection possible')
        return 'No weather available! :/'


def req_mvg_departures(station_id: int) -> dict:
    """ Making an api call to mvg api and retrieving departures

    :param station_id: mvg id of the desired station
    :return: api response as dict
    """

    # the call returns a list with a dict in it per departure
    return mvg_api.get_departures(station_id)


def get_next_departure(station_id: int) -> dict:
    """ Returns just the next departure for given station

    :param station_id: station to return the next departure for
    :return: dict describing the upcoming departure like time, label and destination
    """
    departure = req_mvg_departures(station_id)[0]

    # convert timestamp in the departure to actual time
    departure['departureTime'] = datetime.fromtimestamp(departure['departureTime']/1000)

    return departure


def get_next_departures(station_id:int, num_departures: int = 4) -> dict:
    """ Returns the next X departures specified

    :param station_id: station id to get the departures for
    :param num_departures: upcoming departures to filter; should be equal since
                            it's not differentiated between directions
    :return: dict holding departure times and direction
    """

    # the api returns a list with an item per departure
    departures = req_mvg_departures(station_id)[0:num_departures]

    # convert all timestamps in the departures to actual time
    for dep in departures:
        dep['departureTime'] = datetime.fromtimestamp(dep['departureTime']/1000)

    return departures


def get_departures(station_ids:List[int], stations_names: List[str],
               num_departures: int = 4) -> dict:
    """ Gets all departures for requested ids

    :param station_ids: list of station ids to get departures for
    :param stations_names: list of station names
    :param num_departures: upcoming departures to filter; should be equal since
                            it's not differentiated between directions
    :return: dict holding departure times and direction per station in the
            format of { id: { station_name, [departures] }, ... }
    """

    if len(station_ids) != len(stations_names):
        return 0

    response = {}
    for station_id, name in zip(station_ids, stations_names):
        response[station_id] = {'station_name': name,
                                'departures': get_next_departures(station_id,
                                                                  num_departures)}

    return response


# ------------------------------------------------------------------------------
# app
def create_app(config):
    """ Sets up the app and returns a runnable instance of it

    """

    app = Flask(__name__)

    @app.route('/')
    def home():

        # get weather info
        key = str(config['weather'].get('openweather_key'))
        city_id = config['weather'].get('city_id')
        units = str(config['weather'].get('units'))

        # holding weather information
        weather = get_weather_info(city_id, key, units)

        # TODO: holding departure information
        # get station ids using [config['...'].split(",")]
        station_ids = config['transport'].get('station_ids').split(', ')
        station_ids = [int(id) for id in station_ids]
        station_names = config['transport'].get('station_names').split(', ')
        num_departures = config['transport'].get('num_departures')
        departures = get_departures(station_ids, station_names, int(num_departures))

        # TODO: hand over remaining time in pretty formmat ?? or maybe handeling in frontend
        # now = datetime.now()
        # for key,val in departures.items():
        #     for dep in val['departures']:
        #         departure_time = dep['departureTime']
        #         #  somehow the minutes of remaining_time is neg
        #         remaining_time = time_to_dep(get_time_diff(start_time=now,
        #                                                    end_time=departure_time))
        #         dep['departureTime'] = remaining_time

        return render_template('index.html', weather=weather, departures=departures)

    # return executable app
    return app
