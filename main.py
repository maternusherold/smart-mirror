#!/usr/bin/env

# ------------------------------------------------------------------------------
# imports
import app
import logging
from argparse import ArgumentParser
from configparser import ConfigParser, ExtendedInterpolation
from os import makedirs, path


# ------------------------------------------------------------------------------
# admin stuff
def true_path(dir: str) -> str:
    if dir.strip() == "":
        return ""
    else:
        return dir
    # return path.abspath(path.join(path.dirname(__file__), dir))


def parse_args(parser: ArgumentParser) -> ConfigParser:
    """ Overrides the default configuration in the provided file with those provided via cli

    :param parser: arguments provided via cli at execution
    :return: final set of parameters as ConfigParser obj
    """

    arg_cli = parser.parse_args()

    # load the provided config file
    ini_parser = ConfigParser(interpolation=ExtendedInterpolation(),
                              converters={'path': true_path})
    ini_parser.read(arg_cli.config_file)

    # delete the config file attribute and change to dict
    delattr(arg_cli, 'config_file')
    arg_cli = arg_cli.__dict__

    # iterate over config file args and check if they were replaced via cli
    for _, section in ini_parser.items():
        for key, _ in section.items():
            if key in arg_cli and arg_cli[key] is not None:
                section[key] = arg_cli[key]

    # return the config parser obj
    return ini_parser


# TODO: change to use logging module
def print_config_contents(parser: ConfigParser):
    """ Prints all contents of the parser obj - just for debugging purposes

    :param parser: ConfigParser obj which contents will be printed
    """

    for _, section in parser.items():
        print('section: ', section)
        for k,v in section.items():
            print('item in {} - {}:{}'.format(section, k, v))


# ------------------------------------------------------------------------------
# main 
if __name__ == '__main__':

    # parser reading additional conf arguments from files prefixed with @
    parser = ArgumentParser(fromfile_prefix_chars='@')
    parser.add_argument('-c', '--config-file', default='config.ini',
                        type=str, dest='config_file',
                        help='The configuration file which holds required '
                             'parameters as keys and directories')
    parser.add_argument('-l', '--level', type=str, dest='log_level',
                        choices={'DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'},
                        help='logging level; default is INFO')

    # parse cli and config file
    config = parse_args(parser)

    # check if directories exist
    for path in [config['logging'].getpath('log_dir')]:
        makedirs(path, exist_ok=True)

    # configure logging
    loglevel = getattr(logging, config['logging']['log_level'].upper(), None)

    # logging.basicConfig(filename=config['logging'].getpath('log_file'),
    #                     filemode='w+' if config['logging'].getboolean('overwrite')
    #                                  else 'a+',
    #                     format='%(asctime)s %(message)s',
    #                     level=loglevel)

    # printing when set to verbose
    if config['logging'].getboolean('logging'):
        # TODO: change to using logging instead of print !!
        print_config_contents(config)

    # --------------------------------------------------------------------------
    # execution
    app = app.create_app(config)
    app.run(debug=True)
